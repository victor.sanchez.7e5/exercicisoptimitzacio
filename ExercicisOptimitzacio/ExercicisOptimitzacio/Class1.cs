﻿using System;
using System.Linq;

namespace ExercicisOptimitzacio
{
    public class Class1
    {
        
        static void Main()
        {
            Class1 programa = new Class1();
            programa.Inici();

        }
        
        /*******Inici i Menu *******/
        void Inici()
        {
            bool fi;
            do
            {
                MostrarMenu();
                fi = TractarOpcio();
            } while (!fi);

        }

        void MostrarMenu()
        {
            Console.WriteLine("-----------------");
            Console.WriteLine(" 1.IsLapYear");
            Console.WriteLine(" 2.Minof10Values");
            Console.WriteLine(" 3.HowManyDays");
            Console.WriteLine(" 4.LletraDNI");
            Console.WriteLine(" 5.Finalitzar");
            Console.WriteLine("-----------------");
            Console.WriteLine(" Escull una opcio:");
            Console.WriteLine("");

        }

        public static bool TractarOpcio(){
            int selecion = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("");

            switch (selecion)
            {
                case 1:
                    IsLapYear();
                    break;
                case 2:
                    Minof10Values();
                    break;
                case 3:
                    HowManyDays(); 
                    break;
                case 4 :
                        LletraDni();
                    break; 
                case 5 :
                        return true;
                    
            }

            return false;
            
        }
        
        /******* EXERCICI 1 : Digues si es any de traspas******/

        static void IsLapYear()
        {
            
            Console.Write(" Digues un any del 1 al 2021:");
            int any = Convert.ToInt32(Console.ReadLine());
            
            if(TraspasAny(any)) Console.WriteLine(any+" és any de traspàs"); 
            else Console.WriteLine(any+ " no és any de traspàs");

        }

        public static bool TraspasAny(int any)
        {
            if ((any % 4 == 0 && any % 100 != 0) || (any %4==0 && any%400==0)) return true;
            return false;
        }
        
         /******EXERCICI 2 : El minim entre 10 valors ******/
         
         static void Minof10Values()
         {
             int[] array; 
             array = new int[10];
             int i = 0;

             Console.Write("Escriu el teu vector:");
             Console.Write("-----------------------------------------");

             Console.Write("Per fer el teu vector escriu 10 numeros :");
             LlegeixArr(i, array); 
             
             Console.Write("Vector final: ");
             TornaVector(i, array);
             Console.WriteLine("El mínim és " + array.Min());  
         }

         public static void LlegeixArr(int i, int [] vector)
         {
             for (i = 0; i < vector.Length; i++)
             {
                 Console.Write("Numero del vector - {0} : ", i);
                 vector[i] = Convert.ToInt32(Console.ReadLine());
             }
         }

         public static void TornaVector(int i , int [] vector)
         {
             for (i = 0; i < vector.Length; i++) Console.Write("{0}  ", vector[i]);
         }
         
          
         
         /******EXERCICI 3 : Dies del Mes  ******/

         static void HowManyDays()
         {
             
             string mes;
             int any;
             string mesT;
             
             Console.Write(" Digues un any del 1 al 2021:");
             any = Convert.ToInt32(Console.ReadLine());

             if (AnyTraspas(any))
             {
                 Console.Write(" Digues el mes del any en minúscules :");
                 mesT = Convert.ToString(Console.ReadLine());
                 if (!string.IsNullOrEmpty(mesT = "febrer")) Console.Write("El mes de " + mesT + " té " + 29 + " dies.");
                 Console.WriteLine("");
             } else{
                 Console.Write(" Digues el mes del any en minúscules :");
             mes = Convert.ToString(Console.ReadLine());
             Console.Write("El mes de " + mes + " té "+ quinmes(mes) + " dies.");
             Console.WriteLine("");
             }
         }

         static string quinmes(string mes)
         {
             string diames = null; 
             
             switch (mes)
             {
                 case "gener" :
                     diames = "31";
                     break;
                 case "febrer":
                     diames = "28";
                     break;
                 case "març" :
                     diames = "31"; 
                     break;
                 case "abril":
                     diames = "30"; 
                     break;
                 case "maig" :
                     diames = "31"; 
                     break;
                 case "juny" :
                     diames = "30"; 
                     break;
                 case "juliol" :
                     diames = "31"; 
                     break;
                 case "agost" :
                     diames = "31"; 
                     break;
                 case "septembre" :
                     diames = "30"; 
                     break;
                 case "octubre" :
                     diames = "31"; 
                     break;
                 case "novembre" :
                     diames = "30"; 
                     break;
                 case "desembre" :
                     diames = "31"; 
                     break;
             }
             return diames; 
         }

         static bool AnyTraspas(int any)
         {
             if ((any % 4 == 0 && any % 100 != 0) || (any %4==0 && any%400==0)) return true;
             return false;
         }
         
         /******EXERCICI 4 : DNI ******/
         static void LletraDni()
        {
            Console.WriteLine("Programa Lletra DNI ");
            Console.WriteLine("----------------------- ");
            Console.WriteLine("Digues les 8 xifres del teu DNI ");
            string xifres = Convert.ToString(Console.ReadLine());      /*S'introdueix el DNI per xifres */

            while (EntrarNumPositiu(xifres)) 
            {
                Console.WriteLine("El DNI ha de ser un enter de 8 dígits:");
                xifres = Convert.ToString(Console.ReadLine());
            }
            Console.WriteLine("El teu DNI és :" + ObtencioDNI(xifres)); 
            
            
        }

         static bool EntrarNumPositiu(string xifresdni)
        {
            while (xifresdni != null  && xifresdni.Length !=8) return true;           
            return false;
        }

        static string ObtencioDNI (string xifres )
        {
            int codilletra;
            int dni = Convert.ToInt32(xifres); /*com que les xifres del dni son un string les passem a INT per poder treure el codi */
            string dnicomplet = Convert.ToString(dni); /*fem que la variable DNI passi a ser string per poder afegir-hi la lletra*/
          
            char[] lletraDni = {'T', 'R', 'W', 'A', 'G', 'M', 'Y', 'F', 'P', 'D', 'X', 'B','N','J','Z','S','Q','V','H','L','C','K','E'};

            
            codilletra = dni % 23;        /*el resultat del valor que indiquem a la taula (array) */
            dnicomplet  += lletraDni[codilletra];
            
            return dnicomplet;
            
        }


        public static bool? LlegeixArr(int i)
        {
            throw new NotImplementedException();
        }
    }
}

