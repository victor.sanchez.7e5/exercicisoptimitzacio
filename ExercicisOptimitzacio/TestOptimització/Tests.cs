﻿using ExercicisOptimitzacio;
using NUnit.Framework;

namespace TestOptimització
{
    [TestFixture]
    public class Tests
    {
        
        /*TEST ISLAPYEAR*/
        
        [Test]
        public void Anytraspas()
        {
            Assert.IsTrue(Class1.TraspasAny(2020));
        }
        
        [Test]
        public void Anynotraspas()
        {
            Assert.IsFalse(Class1.TraspasAny(2015));
        }
        
        /*Test MIN OF 10 VALUES */
        [Test]
        public void Vectorincorrecte()
        {
            Assert.IsNotNull(Class1.LlegeixArr(i:));
        }

        
    }
}
