using System;

namespace Exercicis_Recursivitat
{
    public class InData
    {
        public void DataSerieDef()
        {
            
            /*Dades del Ex 1 */
            int n = Convert.ToInt32(Console.ReadLine());
            
            Console.WriteLine(Recursivity.SerieDefinida(n-1));
            
        }
        
        public void DataDivis()
        {
            
            /*Dades del Ex 1 */
            int dividend = Convert.ToInt32(Console.ReadLine());
            int divisor = Convert.ToInt32(Console.ReadLine());
            int quocient = 1 ;
            
            Console.WriteLine(Recursivity.DivisionResta(dividend,divisor,quocient));
            
        }

        public void DataComb()
        {
            int m = Convert.ToInt32(Console.ReadLine());
            int n = Convert.ToInt32(Console.ReadLine());
            int numcombi = 0; 
            Console.WriteLine(Recursivity.Combinacions(m,n,numcombi));

        }

        public void DataVectorSum()
        {
            /*un vector de n elements*/
            int n = Convert.ToInt32(Console.ReadLine()); 
            
            Random vector = new Random();
            char[] posiblecombinacio = {'A', 'B', 'C', 'D', 'E', 'F'}; 
            int rndsecret = vector.Next (posiblecombinacio.Length); 
        }
        
    }
}