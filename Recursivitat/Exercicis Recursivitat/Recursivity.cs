namespace Exercicis_Recursivitat
{
    public class Recursivity
    {
        public static int SerieDefinida(int i)
        {
            
            if (i > 1)
            {
                return 3 * SerieDefinida(i-1)+ 2* SerieDefinida(i-2); 
            }
            else if (i == 1)
            {
                return 1; 
            }
            else
            {
                return 0; 
            }
            
        }

        public static int DivisionResta(int dividend, int divisor, int quocient)
        {
            if (dividend > divisor)
            {
                quocient++;
                return DivisionResta(dividend - divisor,divisor,quocient );
            }
            else
            { 
                return quocient; 
            }
        }
            
        public static int  Combinacions(int m, int n, int numcombi)
        {
            
            if (n >= 1 && 0 <= m && 0 <= n)
            {
                
                if (m == 0 || m == n || n == 1)
                {
                    numcombi++;
                    return 1; 
                }
                else
                {
                    numcombi++; 
                    return Combinacions(n - 1, m,numcombi) + Combinacions(n - 1, m - 1,numcombi); 
                }
            }

            return numcombi; 
        }

        public static void VectorSum()
        {
            
        }
    }
}
