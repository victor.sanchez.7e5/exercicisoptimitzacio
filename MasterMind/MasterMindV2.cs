using System;

namespace MasterMind
{
    public class Mastermindv2
    {
        public void MasterMindV2()
        {

            char[] guess = new char[4];
            int [] rndsecret = new int[4];
            Random secret = new Random();
            char[] posiblecombinacio = {'A', 'B', 'C', 'D', 'E', 'F'}; 
            
            Console.WriteLine("BENVINGUTS AL MASTERMIND NIVELL MITJÀ");
            Console.WriteLine("HAS D'ESCRIURE 4 D'AQUESTES LLETRES EN MAJÚSCULA SENSE REPETIR CAP: 'A', 'B', 'C', 'D', 'E', 'F' ");
            
            /*creació num secret*/
            for (int i = 0; i < 4; i++)
            {
                bool afegeixnum;
                
                do
                {
                    
                    rndsecret [i]= secret.Next(posiblecombinacio.Length); 
                    afegeixnum = false;
                    for (int j = 0; j < i; j++)
                    {
                        if (rndsecret[i] == rndsecret[j])
                        {
                            afegeixnum = true; 
                        }
                    }

                } while (afegeixnum); 
                
                /*Compruebo los valores secret que me da*/
                Console.Write(posiblecombinacio[rndsecret[i]]);
            }
            
            
            int posicioCorrecta;
            int posicioIncorrecta;
            int torn = 0;
            int compta;
            bool guanyador = false;

            
                /*Iteració de TORNS EN LA PARTIDA */
                for (; torn <= 12 && !guanyador; torn++)
                {

                    posicioCorrecta = 0;
                    posicioIncorrecta = 0;
                    compta = 0;

                    Console.WriteLine("-----------------");
                    Console.WriteLine("Torn: {0}", torn);
                    Console.WriteLine("Escriu la combinació que creus correcta :");
                    Console.Write(">>");

                    /*string a = Console.ReadLine().ToUpper(); 
                    string b = Console.ReadLine().ToUpper(); 
                    string c = Console.ReadLine().ToUpper(); 
                    string d = Console.ReadLine().ToUpper(); 
                    
                    char [] hola = a.ToCharArray();*/
                    guess[0] = Convert.ToChar(Console.Read());
                    guess[1] = Convert.ToChar(Console.Read());
                    guess[2] = Convert.ToChar(Console.Read());
                    guess[3] = Convert.ToChar(Console.Read());

                    /* Que nomes sigui possible escriure les lletres de la A a la F */
                    for ()
                    {
                        
                        while (guess[i] )
                        {
                            Console.ForegroundColor = ConsoleColor.DarkRed;
                            Console.WriteLine("     Has d'escriure lletres en majúscula de la A a la F :");
                            Console.ForegroundColor = ConsoleColor.White;
                            Console.Write(">>");
                            guess[0] = Convert.ToChar(Console.Read());
                            guess[1] = Convert.ToChar(Console.Read());
                            guess[2] = Convert.ToChar(Console.Read());
                            guess[3] = Convert.ToChar(Console.Read());
                        }
                    }
                    
                /* Mentre que tinguem lletres repetides demana una nova combinació */ 
                
                while (guess[0] == guess[1] || guess[0] == guess[2] || guess[0] == guess[3] ||
                       guess[1] == guess[2] || guess[1] == guess[3] || guess[2] == guess[3])
                        {
                            Console.ForegroundColor = ConsoleColor.DarkRed;
                            Console.WriteLine("     No es poden Repetir lletres, escriu una nova combinació : ");
                            Console.ForegroundColor = ConsoleColor.White;
                            Console.Write(">>");
                            guess[0] = Convert.ToChar(Console.Read());
                            guess[1] = Convert.ToChar(Console.Read());
                            guess[2] = Convert.ToChar(Console.Read());
                            guess[3] = Convert.ToChar(Console.Read());

                        }
                        
                        /*comprova posicions correctes i les incorrectes */

                        for (; compta < 4; compta++)
                        {
                            if (posiblecombinacio[rndsecret[compta]] == guess[compta])
                            {
                                posicioCorrecta++;
                                /*funciona quan la posició és correcta*/
                            }
                            
                            for (int j = 0; j < 4; j++)
                            {
                                if (posiblecombinacio[rndsecret[compta]] == guess[j]&&posiblecombinacio[rndsecret[compta]] != guess[compta])
                                {
                                    posicioIncorrecta++;
                                    /*TROBA LES POSICIONS INCORRECTES PERO TAMBÉ COMPTA LES CORRECTES*/
                                }

                            }
                            
                            
                        }
                        Console.ForegroundColor = ConsoleColor.Green;
                        Console.Write("---Posició correcta = {0} ", posicioCorrecta);
                        Console.ForegroundColor = ConsoleColor.Red;
                        Console.WriteLine(", Posició erronia = {0}",posicioIncorrecta);
                        Console.ForegroundColor = ConsoleColor.White;
                        /*Comprova el cas en que guanya */
                        if (posiblecombinacio[rndsecret[0]] == guess[0] &&
                            posiblecombinacio[rndsecret[1]] == guess[1] &&
                            posiblecombinacio[rndsecret[2]] == guess[2] &&
                            posiblecombinacio[rndsecret[3]] == guess[3])
                        { 
                            Console.ForegroundColor = ConsoleColor.Yellow;
                            Console.WriteLine("👍Enhorabona! has guanyat!!!!! 👍");
                            Console.ForegroundColor = ConsoleColor.White;
                            guanyador = true;
                            
                        }
                }
                
        Console.ReadLine();

        }
    }
}