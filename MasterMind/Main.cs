using System;
using System.Diagnostics;

namespace MasterMind
{
    internal class MAIN 
    {
        public static void Main()
        {
            MAIN a = new MAIN();
            a.Inici(); 
        }
        
        void Inici()
        {  
            bool fi;
            do
            {
                MENU b = new MENU();
                b.Menu();
                fi = b.EligeMenu (); 

            } while (!fi);
        }
        
    }
}