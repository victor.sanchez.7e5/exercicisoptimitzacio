/*
 
Autor : Victor Sánchez 
Data  19-11-2021

*/


using System;

namespace MasterMind
{
    public class MASTERMINDV1
    {
        public void MasterMindV1()
        {
            
            
            char[] guess = new char[4];
            int [] rndsecret = new int[4];
            Random secret = new Random();
            char[] posiblecombinacio = {'A', 'B', 'C', 'D', 'E', 'F'}; 

            
            /*creació num secret*/
            for (int i = 0; i < 4; i++)
            {
                bool afegeixnum;
                
                do
                {
                    
                    rndsecret [i]= secret.Next(posiblecombinacio.Length); 
                    afegeixnum = false;
                    for (int j = 0; j < i; j++)
                    {
                        if (rndsecret[i] == rndsecret[j])
                        {
                            afegeixnum = true; 
                        }
                    }

                } while (afegeixnum); 
                
                /*Compruebo los valores secret que me da*/
                Console.Write(posiblecombinacio[rndsecret[i]]);
            }
            
            
            int posicioCorrecta = 0;
            int posicioIncorrecta = 0;
            int torn = 0;
            int compta = 0;
            
                /*Iteració de comprovació */ 
                for (; torn <= 12 && posicioCorrecta < 4; torn++)
                {
                    
                    Console.WriteLine("");
                    Console.WriteLine("Torn: {0}", torn);
                    Console.WriteLine("Escriu la combinació que creus correcta :");
                    guess[0] = Convert.ToChar(Console.Read());
                    guess[1] = Convert.ToChar(Console.Read());
                    guess[2] = Convert.ToChar(Console.Read());
                    guess[3] = Convert.ToChar(Console.ReadLine());
                    
                    /*Comprova el cas en que guanya */ 
                        if (posiblecombinacio[rndsecret[0]] == guess[0] && posiblecombinacio[rndsecret[1]]== guess[1] && posiblecombinacio[rndsecret[2]] == guess[2] &&
                            posiblecombinacio[rndsecret[3]] == guess[3])
                        {
                            Console.WriteLine("Enhorabona! has guanyat!!!!! ");
                        }
                        
                }
                Console.WriteLine("Posició correcta = {0} , Posició erronia = {1}", posicioCorrecta, posicioIncorrecta);

                Console.ReadLine(); 
        }

        
    }
}