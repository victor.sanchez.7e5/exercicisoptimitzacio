﻿/*
 * Author : Víctor Sánchez
 * Date : 6-12-2021
 * Description : Exercicis d'arrays unidimensional 
 */
using System;

namespace Array_Unidimensional
{
    internal class Program
    {

        static void Menu()
        {
            bool salir = false;

            /*MENU DELS ARRAY UNIDIMENSIONALS*/
            
            while (!salir)
            {
                Console.WriteLine("");
                Console.WriteLine("0-DayOfWeek");
                Console.WriteLine("1-PlayerNumbers");
                Console.WriteLine("2-CandidatesList");
                Console.WriteLine("5-Salir");
                Console.WriteLine("");
                Console.WriteLine("Elige el ejercicio que quieres ejecutar:");
                int opcion = Convert.ToInt32(Console.ReadLine());
                Console.WriteLine("");
                
                switch(opcion)
                {
                    case 0:
                        DayOfWeek(); 
                        break;
                    case 1 :
                        PlayerNumbers();
                        break;
                    case 2:
                        CandidatesList();
                        break; 
                    case 5:
                        salir = true; 
                        break;
                }
            }
        } /*fin del menu*/
        
        public static void Main()
        {
            Menu(); 
        } 

        static void DayOfWeek()
        {
            string [] daysweek ;
            daysweek = new string [7]{ "Dilluns ", "Dimarts", "Dimecres", "Dijous", "Divendres", "Dissabte", "Diumenge"};
            var dia = Convert.ToInt32(Console.ReadLine());
            dia -= 1;
            Console.WriteLine(daysweek[dia]);
            
        }

        static void PlayerNumbers()
        {
            int[] saveplayers;
            saveplayers = new int [5];
            
            for (int i = 1; i < 6 ; i++)
            {
                
                Console.WriteLine("Escriu el número del jugador " + i);
                saveplayers[i-1] = Convert.ToInt32(Console.ReadLine());
                
            }
            
            Console.WriteLine("La teva aliniació és : ");
            Console.Write("[");

            for (int j = 0; j < 5; j++)
            {
                Console.Write( saveplayers[j] +","); /*falta treure la coma del final */ 
                
            }
            Console.Write("]");
            
        }

        static void CandidatesList()
        {
            char[] candidates;
            int numcandidats; 
            candidates = new char[]{};
            
            do
            {
                Console.WriteLine("Escriu el número de candidats a escollir :");
                numcandidats = Convert.ToInt32(Console.ReadLine());
                
                for (int i = 0; i < candidates.Length ; i++)
                {
                    candidates[i] = Convert.ToChar(Console.ReadLine());
                
                }
            } while (numcandidats > 0);         /*per acabar*/
            
        }

        static void LetterInWord()
        {
            string[] word;
            word = new string[] {};
            
        }
        
    }
    
}
