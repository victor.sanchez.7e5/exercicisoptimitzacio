﻿
/****
 * Author : Víctor Sánchez
 * Data : 13-12-2021
 * Description : Exercicis d'arrays multidimensional  
 ****/


using System;
using System.Dynamic;

namespace Array_Multidimensional
{
    public class Program
    {

        static void Main()
        {
            Menu();
        }

        static void Menu()
        {
            bool salir = false;

            while (!salir)
            {  
                Console.Clear();
                Console.WriteLine("0-SimpleBattleshipResult");
                Console.WriteLine("1-MatrixElementSum");
                Console.WriteLine("2-MatrixBoxesOpenedCounter");
                Console.WriteLine("3-MatrixIsThereADiv13");
                Console.WriteLine("4-HighestMountainOnMap");
                Console.WriteLine("5-HighestMountainScaleChange");
                Console.WriteLine("6-MatrixSum");
                Console.WriteLine("7-RookMoves");
                Console.WriteLine("8-Salir");
                Console.WriteLine("Elige una de las opciones");
                int opcion = Convert.ToInt32(Console.ReadLine());

                switch (opcion)
                {
                    case 0:
                        SimpleBattleshipResult();
                        break;
                    case 1:
                        MatrixElementSum();
                        break;
                    case 2:
                        MatrixBoxesOpenedCounter();
                        break;
                    case 3:
                        MatrixIsThereADiv13();
                        break;
                    case 4:
                        HighestMountainOnMap();
                        break;
                    case 5:
                        HighestMountainScaleChange();
                        break;
                    case 6:
                        MatrixSum();
                        break;
                    case 7:
                        RookMoves();
                        break;
                    case 8: 
                        salir = true;
                        break;
                }
            }
        }

        static void SimpleBattleshipResult()
        {
            Console.WriteLine("Benvingut al Joc d'enfonsar la flota !");
            Console.WriteLine("");
            Console.WriteLine("Indica unes coordenades d'entre 0-3 en files i 0-6 en columnes");

            char[,] tauler;
            tauler = new[,]
            {
                {'x', 'x', '0', '0', '0', '0', 'x'},
                {'0', '0', 'x', '0', '0', '0', 'x'},
                {'0', '0', '0', '0', '0', '0', 'x'},
                {'0', 'x', 'x', 'x', '0', '0', 'x'}
            };

            int x;
            int y;
            do
            {
                Console.WriteLine("El valor ha de ser entre 0-3 en files i 0-6 en columnes!!!");
                x = Convert.ToInt32(Console.ReadLine());
                y = Convert.ToInt32(Console.ReadLine());

            } while ((x > tauler.Length) || (x < tauler.Length) || (x > tauler.Length) || (y < tauler.Length));

            if (tauler[x, y] == 'x')
            {
                Console.WriteLine("Tocat!");
                Console.WriteLine("");
                Console.WriteLine("");
            }
            else
            {
                Console.WriteLine("Aigua");
                Console.WriteLine("");
                Console.WriteLine("");
            }

        }       /*** SimpleBattleshipResult ***/

        static void MatrixElementSum()
        {
            Console.WriteLine("Tenim la matriu {{2,5,1,6},{23,52,14,36},{23,75,81,64}}");

            int[,] matrix = {{2, 5, 1, 6}, {23, 52, 14, 36}, {23, 75, 81, 64}};
            int suma = 0;
            for (int i = 0; i < matrix.GetLength(0); i++)
            {
                for (int j = 0; j < matrix.GetLength(1); j++)
                {
                    suma += matrix[i, j];
                }
            }

            Console.WriteLine("I el resultat de la suma de la matriu {{2,5,1,6},{23,52,14,36},{23,75,81,64}} és :");
            Console.WriteLine("");
            Console.WriteLine("{0}", suma);

            Console.Read();
        }       /*** MatrixElementSum ***/

        static void MatrixBoxesOpenedCounter()     
        {
            /*
            int[,] caixa = new int [3, 4];
            int x;
            int y;
            int i;
            int j;
            Console.WriteLine("Introdueixi dos enters entre 0 i 3 per indicar la posició de la caixa que s'ha obert i en finalitzar escriu -1");
            
            do
            {
                x = Convert.ToInt32(Console.Read());
                y = Convert.ToInt32(Console.Read());
                caixa[x, y]++;
                
            } while (x != -1);

            Console.WriteLine("A continuació es mostrara en pantalla les vegades que s'ha obert cada caixa : ");

            for (i = 0; i < caixa.GetLength(0); i++)
            {

                for (j = 0; j < caixa.GetLength(1); j++)
                {
                    Console.Write("{0}\t", caixa[i, j]);
                }

                Console.WriteLine();
            }

            Console.Read();
            */ 
        }

        static void MatrixIsThereADiv13()
        {
            int[,] matrix = {{2, 5, 1, 6}, {23, 52, 14, 36}, {23, 75, 81, 62}};
            int i;
            int j;
            bool isdiv = false;

            Console.WriteLine("La matriu {{2,5,1,6},{23,52,14,36},{23,75,81,62}} és divisible entre 13 ?");

            for (i = 0; i < matrix.GetLength(0); i++)
            {
                for (j = 0; j < matrix.GetLength(1); j++)
                {
                    if (matrix[i, j] % 13 == 0) isdiv = true;
                }
            }

            if (isdiv)
            {
                Console.WriteLine("True");
            }
            else
            {
                Console.WriteLine("False");
            }

            Console.Read();
        }       /*** MatrixIsThereADiv13 ***/

        static void HighestMountainOnMap()
        {
            /*
            Console.WriteLine(
                "Amb les coordenades en el mapa de bits : {1.5, 1.6, 1.8, 1.7, 1.6}, {1.5, 2.6, 2.8, 2.7, 1.6}, {1.5, 4.6, 4.4, 4.9, 1.6},{2.5, 1.6, 3.8, 7.7, 3.6}, {1.5, 2.6, 3.8, 2.7, 1.6}");

            double[,] map =
            {
                {1.5, 1.6, 1.8, 1.7, 1.6}, {1.5, 2.6, 2.8, 2.7, 1.6}, {1.5, 4.6, 4.4, 4.9, 1.6},
                {2.5, 1.6, 3.8, 7.7, 3.6}, {1.5, 2.6, 3.8, 2.7, 1.6}
            };

        */

            Console.Read();
        }

        static void HighestMountainScaleChange()
        {
             double[,] map =
                        {
                            {1.5, 1.6, 1.8, 1.7, 1.6}, {1.5, 2.6, 2.8, 2.7, 1.6}, {1.5, 4.6, 4.4, 4.9, 1.6},
                            {2.5, 1.6, 3.8, 7.7, 3.6}, {1.5, 2.6, 3.8, 2.7, 1.6}
                        };
             double[,] pies = new double [5, 5];

             for (int i = 0; i < map.GetLength(0); i++)
             {
                 Console.WriteLine();
                 for (int j = 0; j > map.GetLength(1); j++)
                 {
                     pies[i, j] = (map[i, j] * 3.2808); 
                     Console.WriteLine(pies[i,j] + "\t");
                 }
             }
             Console.Read();
        }       /*** HighestMountainScaleChange ***/

        static void MatrixSum()
        {
            /*
            var matriu1 = new int[3, 4];
            int x; 
            var y=0;
            int i;
            int j;
            
            Console.WriteLine("Escriu combinacions de dos enters del 0 al 3 (caixes de seguretat) i quan acabis escriu -1 :");
            
            
            for (i = 0; i < matriu1.GetLength(0); i++)
            {
                
                for (j = 0; j < matriu1.GetLength(1); j++)
                {
                    caixa[3, 4] = Convert.ToInt32(Console.ReadLine()); 
                    Console.Write("{0}\t",matriu1[i,j]);
                }
                Console.WriteLine();
            }
            

            Console.Read(); 
        }
        */
            
        }
        
        static void RookMoves()
        {
            /*
            string[,] tauler = new string[8,8];

            for (int i = 0; i < tauler.GetLength(0); i++)
            {
                for (int j = 0; j < tauler.GetLength(1); j++)
                {
                    tauler[i, j] = "x"; 
                }
            }
            Console.WriteLine("Indica a quina posició vols moure la torre : ");
            Console.Read();
            */
        }
    }
}
