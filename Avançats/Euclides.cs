/**************
 Author: Víctor Sánchez
 Data : 5/11/2021
 Descripció : Implementa l'algorisme d'Euclides, que calcula le màxim comú divisor de dos nombres positius.
******/
/*
using System; 

namespace Avançats
{
    public class Euclides
    {
        public static void Main(string[] args)
        {
            Console.Write("Escriu un enter Postitiu: ");
            int a = Convert.ToInt32(Console.ReadLine());
            Console.Write("Escriu un altre enter Postitiu: ");
            int b = Convert.ToInt32(Console.ReadLine());
            int gran = 0;
            int petit = 0;
            int residu = 0; 
            int resultat = 0; 
            
            if (a>b)
            {
                gran = a;
                petit = b;
            } else
            {
                gran = b;
                petit = a; 
            }

            while (petit != 0)
            {
                residu = gran % petit;
                gran = petit;
                petit = residu;
            }
            
            
            if (residu == 0)
            {
                resultat = gran;
                Console.Write("{0}", resultat);
            }
        }
    }
}

*/