/**************
 Author: Víctor Sánchez
 Data : 5/11/2021
 Descripció : Donat un nombre enter positiu, escriu els nombres de Fibonacci (Lleonard de Pisa) inferiors o iguals a ell.
Els nombres de Fibonacci es defineixen de la manera següent: El primer és 0, el segon és 1, el següent és la suma dels dos anteriors i així successivament.
**************/
/*
using System;

namespace Avançats
{
    public class Fibonacci
    {
        public static void Main(string[] args)
        {
                
            Console.Write("Introdueix un nombre enter positiu : ");
            int enter = Convert.ToInt32(Console.ReadLine());
            int a = 0;
            int b = 1;
            int fibonucci = 0; 
                
            Console.WriteLine("0,1");

            for (fibonucci = a + b;fibonucci < enter; fibonucci++)
            {
                fibonucci = b + fibonucci; 
                Console.Write(",{0}",fibonucci);
            }
                
				
				
        }
            
    }
}
*/