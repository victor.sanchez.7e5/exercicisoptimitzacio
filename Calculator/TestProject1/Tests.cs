﻿using System;
using Calculator;
using NUnit.Framework;

namespace TestProject1
{
    [TestFixture]
    public class Tests
    {
        [Test]
        
        public void TestSuma()
        {
            Assert.AreEqual(42, Class1.Add(40,2));
        }

        [Test]
        public void TestResta()
        {
            Assert.AreEqual(expected:10, actual:Class1.Substract(21,11));

        }

        [Test]
        public void TestProduct()
        {
            Assert.AreEqual(expected:12, actual:Class1.Product(3,4));
        }
        
        [Test]
        public void TestDivision()
        {
            Assert.AreEqual(expected:5, Class1.Division(15,3));
        }


/*        [Test]

        public void FailingTest()

        {

            Assert.AreEqual(5, Class1.Add(2,2));

        }
        */

        public void Test1()
        {
            Assert.True(true);
        }
    }
}


