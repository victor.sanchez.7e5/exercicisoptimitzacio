using System;

namespace ExercicisParametrització
{
    public class Righttrianglesize 
    {
        public void Rigthtrianglesize()
        {
            

            Console.WriteLine("Digues l'àrea i el perímetre d'una llista de triangles rectangles");
            Console.WriteLine("1-- Introdueix el número de triangles");
            
            int triangles = Convert.ToInt32(Console.ReadLine());
             
            int i;
            double[] labase = new double[triangles];
            double[] altura = new double[triangles];
            
            Console.WriteLine("2-- Entra la base i l'altura del triangle.");
            
            for (i = 0; triangles > i ; i++ ) {
                Console.WriteLine("Base:");
                labase[i] =  Convert.ToDouble(Console.ReadLine());
                Console.WriteLine("Altura:");
                altura[i] =  Convert.ToDouble(Console.ReadLine());
                Console.WriteLine("");
            }
            
            for (int j = 0; triangles > j; j++)
            {
                Console.WriteLine("Un triangle de " + labase[j] + " x " + altura[j] + " té " + CalculArea(labase[j], altura[j]) +
                                  " de area i " + CalculPerimetro(labase[j], altura[j]) + " de perímetre. ");
                                  
            }

            Console.ReadLine(); 
        }
        
        /*metode llegeixtriangle
        private void llegeixtriangle(int triangles,double labase,double altura)
        {
            
            
            
        }
        */
        
        private double CalculArea(double labase, double laltura)
        {
            double area; 
            area = (labase * laltura) / 2;
            return area; 
        }

        private double CalculPerimetro(double labase, double laltura)
        {
            double hipotenusa;
            double perimetro;
            hipotenusa = Math.Sqrt((labase * labase) + (laltura * laltura));
            perimetro = labase + laltura + hipotenusa;
            return perimetro;

        }
        
    }
}