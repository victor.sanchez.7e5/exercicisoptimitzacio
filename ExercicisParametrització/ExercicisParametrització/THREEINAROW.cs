using System;
using System.IO;

namespace ExercicisParametrització
{
    public class THREEINAROW
    {
        public void TresEnRaya()
        {
            string[,] tablero = 
            {
/* Posició :      0    1    2    3  */
                {"", "v", "v", "v"},
                {">", "_", "_", "_"},
                {">", "_", "_", "_"},
                {">", "_", "_", "_"}
            };
            
            Console.Write(">");
            int fila = Convert.ToInt32(Console.ReadLine());
            
            Console.Write(">");
            int columna = Convert.ToInt32(Console.ReadLine());
            
            
            
            MuestraTablero(tablero);
            
            escribeposicion(tablero,columna, fila );
        }
        
        /*Metodo escribe en el tablero */
        public void escribeposicion(string[,] tablero, int columna, int fila)
        {
            string column = Convert.ToString(columna);
            string row = Convert.ToString(fila);
            var s = tablero[fila, columna];
        }
        
        public void MuestraTablero(string[,] tablero)
        {
            /*                  C       O       L       U       M       N       A       S
/*F     */   Console.WriteLine($"{tablero[0,0]} {tablero[0,1]}    {tablero[0,2]}    {tablero[0,3]}");
/*I     */   Console.WriteLine($"{tablero[1,0]} {tablero[1,1]} |  {tablero[1,2]} |  {tablero[1,3]}");
/*L     */   Console.WriteLine($"{tablero[2,0]} {tablero[2,1]} |  {tablero[2,2]} |  {tablero[2,3]}");
/*A     */   Console.WriteLine($"{tablero[3,0]} {tablero[3,1]} |  {tablero[3,2]} |  {tablero[3,3]}");
/*S     */  
  

        }
        
        /*Metodo acaba el juego */
        
        /*Metodo gana X */
        
        /*Metodo gana O */
        
    }
}