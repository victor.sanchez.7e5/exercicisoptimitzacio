﻿using System;

namespace ExercicisParametrització
{
    internal class ParametritzacioMain
    {
        
        /// <summary>
        /// Aquesta classe es la principal dels exercicis de parametrització 
        /// </summary>
        public static void Main()
        {
            ParametritzacioMain programa = new ParametritzacioMain();
            programa.Inici();
            
        }

        /*******Inici i Menu *******/
        /// <summary>
        /// Aqui trobem el mètode que inicia el programa i el seu menu
        /// </summary>
        void Inici()
        {
            bool fi;
            do
            {
                ErManu();
                fi = EligeManu();
            } while (!fi);

        }
        void ErManu()
        {
            
          Console.WriteLine("Menú dels Exercicis de Parametrització: ");
          Console.WriteLine("--------------------------------------- ");
          Console.WriteLine("1-RightTriangleSize");
          Console.WriteLine("2-Lamp");
          Console.WriteLine("3-CampSiteOrganizer");
          Console.WriteLine("4-BASICROBOT");
          Console.WriteLine("5-Tres en Raya");
          Console.WriteLine("6- Fi del Programa");
          Console.WriteLine("");
          
        }
        
        /// <summary>
        /// Mètode que llegeix de teclat l'opció que escull l'usuari
        /// </summary>
        /// <param name="fin"></param>
        public static bool EligeManu()
        {
            int opcio = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("");
            
            switch (opcio)
            {
                case 1:
                    Righttrianglesize haz = new Righttrianglesize();
                    haz.Rigthtrianglesize();
                    break;
                case 2:
                    LAMP fes = new LAMP();
                    fes.Lamp();
                    break;
                case 3:
                   CAMPSITEORGANIZER has = new CAMPSITEORGANIZER();
                    has.CampSiteOrganizer();
                    break;
                case 4 :
                    BASICROBOT hace = new BASICROBOT();
                    hace.BasicRobot();
                    break; 
                case 5 :
                    THREEINAROW z = new THREEINAROW(); 
                    z.TresEnRaya();
                    break;
                case 6 :
                    return true;
                    
            }
            return false;
        }
    }
}


