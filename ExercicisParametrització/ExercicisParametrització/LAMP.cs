using System;
using System.Globalization;

namespace ExercicisParametrització
{
    public class LAMP
    {
        public void Lamp()
        {
            Console.WriteLine("Simulador d' una làmpada, amb les accions:");
            Console.WriteLine("Escriu TURN ON per encedre la làmpada");
            Console.WriteLine("Escriu TURN OFF per apagar la làmpada");
            Console.WriteLine("Escriu TOGGLE per canviar l'estat de la làmpada");
            Console.WriteLine("Escriu END per finalitzar el programa");
            Console.WriteLine("");
            TractarOpcio(); 
        }
        
        public void TractarOpcio()
        {
            var  fi = false;
            bool luz = false;
            
            /*Bucle missatges llum */
            do
            {
                 string opcio = Console.ReadLine();

                switch (opcio.ToUpper())
                {
                    case "TURN ON":
                        encenllum();
                        luz = true;
                        break;
                    case "TURN OFF":
                        apagallum();
                        luz = false;
                        break;
                    case "TOGGLE":
                        canviallum(luz);
                        break;
                    case "END":
                        Console.WriteLine("");
                        fi = true;
                        break;
                }
            } while (!fi);
        }

        public void encenllum()
        {
            
                Console.WriteLine("Llum Encesa");
                Console.WriteLine("");
            
            
        }

        public void apagallum()
        {
            Console.WriteLine("Llum Apagada");
                Console.WriteLine("");
        }
        
        public void canviallum(bool luz)
        {
            if (!luz)
            {
                Console.WriteLine("Llum Encesa");

            }
            else
            {
                Console.WriteLine("Llum Apagada");

            }
            Console.WriteLine("");
            
            
        }
    }
}