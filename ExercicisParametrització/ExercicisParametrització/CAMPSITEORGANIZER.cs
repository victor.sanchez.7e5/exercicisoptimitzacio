using System;
using System.Linq;

namespace ExercicisParametrització
{
    public class CAMPSITEORGANIZER
    {
        public void CampSiteOrganizer()
        {
            Console.WriteLine("Quan s'ocupi una parcel·la introdueix ENTRA, el número de persones i el nom de la reserva");
            Console.WriteLine("Quan es buidi una parcel·la introdueix MARXA i el nom de la reserva");
            Console.WriteLine("Escriu END per finalitzar el programa");
            Console.WriteLine("");
            TractarOpcio();
        }
        
        void TractarOpcio()
        {
            var  fi = false;
            bool demanantdades = false;
            int i = 1; 
            int [] Numpersones = new int [i];
            string [] Nomreserva = new String[i];
            
            
            /*Bucle missatges ORGANITZACIÓ */
            do
            {
                Console.Write(">>>");

                string opcio = Console.ReadLine()?.ToUpper();

                switch (opcio)
                {
                    case "ENTRA":
                        if (i != Numpersones.Length)
                        {
                            Array.Resize(ref Numpersones,i);
                            
                        }
                        Demanadades(Numpersones, Nomreserva, i-1);
                        /*Calcula Gent */
                        i++; 
                        Console.WriteLine("Persones: "+Numpersones.Sum());
                        /*Calcula Gent */
                        Console.WriteLine("");
                        imprimeixarray(Numpersones);

                        break;
                    case "MARXA":
                        
                        Console.WriteLine("Adeu");
                        
                        break;
                    case "END":
                        Console.WriteLine("");
                        fi = true;
                        break;
                }
            } while (!fi);
        }

       public void Demanadades(int [] Numpersones, string [] Nomreserva, int i)
        {
            int j = i; 
            Numpersones[j] = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Persones:"+Numpersones[j]);
            Nomreserva[j] = Convert.ToString(Console.ReadLine());
            Console.WriteLine("Nom: "+ Nomreserva[j]);
            
           
        }

        void CalculaGent()
        {
            
        }

        static void imprimeixarray(int[] numper)
        {
            for(int i=0; i<numper.Length; i++)
                Console.WriteLine(i + ": " + numper[i]);
        }
        

    }
}