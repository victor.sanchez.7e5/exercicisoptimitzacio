/*Kichwa : basic robot
 Author: Víctor Sánchez
 Fecha : 1-02-2022
 Descrpció : El robot ha de guardar la seva posició (X, Y) i la velocitat actual. Per defecte, la posició serà (0, 0) i la velocitat és 1. La velocitat indica la distància que recorre el robot en cada acció.
 */


using System;
using System.Xml.Serialization.Configuration;

namespace ExercicisParametrització
{
    public class BASICROBOT
    {
        public void BasicRobot()
        {
            Console.WriteLine(
                "El robot guardar la seva posició (X, Y) i la velocitat actual. Per defecte, la posició serà (0, 0) i la velocitat és 1. La velocitat indica la distància que recorre el robot en cada acció.");
            Console.WriteLine("El robot té les següents accions:");

            EligeManu();
        }

        public static void EligeManu()
        {
            var  fi = false;
            double[] robot;
            robot = new[] {0.0, 0.0, 1.0};
            
            do
            {
               
                string opcio = Console.ReadLine()?.ToUpper();
                Console.WriteLine("");
                switch (opcio)
                {
                    case "DALT":
                        puja(robot);
                        break;
                    case "BAIX":
                        baixa(robot);
                        break;
                    case "DRETA":
                        dreta(robot);
                        break;
                    case "ESQUERRA":
                        esquerra(robot);
                        break;
                    case "ACCELERAR":
                        accelera(robot);
                        break;
                    case "DISMINUIR":
                        disminueix(robot);
                        break;
                    case "POSICIO":
                        Console.WriteLine("La posició del robot és (" + robot[0] + "," + robot[1] + ")");

                        break;
                    case "VELOCITAT":
                        Console.WriteLine("La velocitat del robot és " + robot[2]);
                        break;
                    case "END":
                        fi = true;
                    break;
                }
            } while (!fi);
        }

        static void puja(double[] robot)
        {
            robot[2] += robot[1];
        }

        static void baixa(double[] robot)
        {
            robot[2] -= robot[1];
        }

        static void dreta(double[] robot)
        {
            robot[2] += robot[0];
        }

        static void esquerra(double[] robot)
        {
            robot[2] -= robot[0];
        }

        static void accelera(double[] robot)
        {
            while (robot[2] < 10)
            {
                robot[2] += 0.5;
            }
        }

        static void disminueix(double[] robot)
        {
            while (robot[2] > 0)
            {
                robot[2] -= 0.5;
            }
        }

    }
}