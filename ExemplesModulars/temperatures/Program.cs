﻿using System;

namespace temperatures
{
    class RegistreTemperatures
    {
        private const int MaxStetmanes = 52; 
        //Variables globals 
        private int _numTemperatures;
        private readonly double[] _temperatures = new double[MaxStetmanes *7];
        private int _dia = 3;
        private int _mes = 1;

        
        
        //Mètodes associats al poblema general 
        static void Main()
        {
            RegistreTemperatures programa = new RegistreTemperatures();
            programa.Inici();
        }

        void Inici ()
        {
            bool fi;
            do
            {
                MostrarMenu();
                fi=TractarOpcio();
            } while (!fi);
        }
        
        //Mètodes associats al primer nivell de descomposició
        void MostrarMenu()
        {
            Console.WriteLine("----------------------------------------------------------------");
            Console.WriteLine(" [RT] Registrar temperatures setmanals.");
            Console.WriteLine(" [MJ] Consultar temperatura mitjana.");
            Console.WriteLine(" [DF] Consultar diferència màxima.");
            Console.WriteLine(" [FI] Sortir");
            
        }
        
        bool TractarOpcio()
        {
         var opcio = Console.ReadLine();
         var  fi = false;
         
         switch (opcio)
         {
             
             case "RT":
                 RegistreTemperaturesSetmanals();
                 break;
             case "MJ" : 
                 MostrarMitjana();
                 break;
             case "DF":
                 MostrarDiferencia();
                 break;
             case "FI" :
                 fi = true; 
                 break;
             default: 
                 Console.WriteLine("Opció incorrecta !\n");
                 break;
         }
         return fi;
        }
        
        //Mètodes associats al segon nivell de descomposició

        void RegistreTemperaturesSetmanals()
        {
            //Cal controlar si hi haurà espai per aquest 7 registres 
            
            if ((_numTemperatures + 7) >= _temperatures.Length)
            {
                Console.WriteLine("No queda espai per a més temperatures.");
            }
            else
            {
                LlegirTemperaturesTeclat();
                IncrementarData();
            }
        }
        
        void MostrarMitjana()
        {
            if (_numTemperatures > 0)
            {
                Console.WriteLine("\nFins avui");
                MostrarData();
                Console.WriteLine(" la mitjana ha estat de ");
                CalculaMitjana();
                Console.WriteLine(" graus.");
            }
            else
            {
                Console.WriteLine("\nNo hi ha temperatures registrades.");
            }
        }

        void MostrarDiferencia()
        {
            if (_numTemperatures > 0)
            {
                Console.WriteLine("\nFins avui");
                MostrarData();
                Console.WriteLine(" la mitjana ha estat de ");
                CalculaDiferencia();            
                Console.WriteLine(" graus.");
            }
            else
            {
                Console.WriteLine("\nNo hi ha temperatures registrades.");
            }
        }
        
        //Mètodes associats al tercer nivell de descomposició
        void LlegirTemperaturesTeclat()
        {
            Console.WriteLine("Escriu les temperatures d'aquesta setmana");
            for (int i = 0; i < 7; i++)
            {
                _temperatures[i] = Convert.ToDouble(Console.ReadLine());
                _numTemperatures++; 

            }
        }
        void IncrementarData()
        {
            //Quants dies té aquest mes ? 
            int diesAquestMes;
            if (_mes == 2)
            {
                diesAquestMes = 28; 
            } else if ((_mes == 4) || (_mes == 6) || (_mes == 9) || (_mes == 11))
            {
                diesAquestMes = 30;
            }
            else
            {
                diesAquestMes = 31; 
            }

            _dia += 7; 
            //Hem passat de mes ?
            if (_dia > diesAquestMes)
            {
                _dia = _dia - diesAquestMes;
                _mes++; 
                
                //Hem passat d'any ? 
                if (_mes > 12)
                {
                    _mes = 1; 
                }
            } 
        }

        void MostrarData()
        {
            Console.WriteLine(_dia + "de");

            switch (_mes)
            {
                case 1: 
                    Console.WriteLine("Gener");
                    break;
                case 2: 
                    Console.WriteLine("Febrer");
                    break;
                case 3: 
                    Console.WriteLine("Març");
                    break;
                case 4: 
                    Console.WriteLine("Abril");
                    break;
                case 5: 
                    Console.WriteLine("Maig");
                    break;
                case 6: 
                    Console.WriteLine("Juny");
                    break;
                case 7: 
                    Console.WriteLine("Juliol");
                    break;
                case 8: 
                    Console.WriteLine("Agosto");
                    break;
                case 9: 
                    Console.WriteLine("Setiembre");
                    break;
                case 10: 
                    Console.WriteLine("Octubre");
                    break;
                case 11: 
                    Console.WriteLine("Novembre");
                    break;
                case 12: 
                    Console.WriteLine("Desembre");
                    break;
            }
        }

        void CalculaMitjana()
        {
            double acumulador = 0;
            for (int i = 0; i < _numTemperatures; i++)
            {
                acumulador = acumulador + _temperatures[i];
            }
            Console.WriteLine((acumulador/_numTemperatures));
        }
        void CalculaDiferencia()
        {
            double max = _temperatures[0];
            double min = _temperatures[0];
            for (int i = 0; i < _numTemperatures; i++)
            {
                if (_temperatures[i] > max) max = _temperatures[i];
                if (min > _temperatures[i]) min = _temperatures[i];
            }
        }
        
    }
}





